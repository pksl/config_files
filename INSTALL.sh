echo "
 _       __     __                             __               __
| |     / /__  / /________  ____ ___  ___     / /_  ____ ______/ /__
| | /| / / _ \/ / ___/ __ \/ __ \__ \/ _ \   / __ \/ __ \/ ___/ //_/
| |/ |/ /  __/ / /__/ /_/ / / / / / /  __/  / /_/ / /_/ / /__/ ,<
|__/|__/\___/_/\___/\____/_/ /_/ /_/\___/  /_.___/\__,_/\___/_/|_|

           _ __       _____ ____
    ____  (_) /______|__  // / /
   / __ \/ / //_/ ___//_ </ / /
  / /_/ / / ,< (__  )__/ / /_/
 / .___/_/_/|_/____/____/_(_)
/_/                             "
sleep 1

echo "[*] Let's make you at home..."
cp .aliases ~/.aliases
source ~/.aliases

echo "[+] Installing haxxor tools"
sudo apt install -y rar gzip curl git tmux apt-transport-https john aircrack-ng
    \ pdfcrack fcrackzip binwalk medusa ufw keepassxc nmap rkhunter
    \ openvpn network-manager-openvpn network-manager-openvpn-gnome network-manager-vpnc
    \ network-manager-vpnc-gnome vlc dconf-cli uuid-runtime

echo "[*] Setting up Github"
echo "[!] Type your pseudo"
read pseudo

echo "[!] Type in your email address (the one used for your GitHub account): "
read email

git config --global user.email $email
git config --global user.name $pseudo
git config --global core.editor "vim"
if hash vim 2>/dev/null; then
    echo "[*] You have vim"
else
    echo "[!] Installing vim"
    sudo apt install vim
fi

cp .vimrc ~/.vimrc
echo "[+] Installing vim plugins..."
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
    curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
cd ~/.vim/bundle/
git clone git@github.com:w0rp/ale.git
git clone git@github.com:ctrlpvim/ctrlp.vim.git
git clone git@github.com:mattn/emmet-vim.git
git clone git@github.com:junegunn/goyo.vim.git
git clone git@github.com:Yggdroot/indentLine.git
git clone git@github.com:itchyny/lightline.vim.git
git clone git@github.com:arcticicestudio/nord-vim.git
git clone git@github.com:tpope/vim-ragtag.git
git clone https://github.com/fatih/vim-go.git
git clone git@github.com:itchyny/vim-gitbranch.git
git clone git://github.com/airblade/vim-gitgutter.git
git clone https://github.com/mxw/vim-jsx.git
git clone git@github.com:ycm-core/YouCompleteMe.git
git clone https://github.com/scrooloose/nerdcommenter.git
git clone https://github.com/scrooloose/nerdtree.git
git clone https://tpope.io/vim/sensible.git
git clone https://github.com/scrooloose/nerdcommenter.git
git clone https://github.com/ayu-theme/ayu-vim.git
git clone https://github.com/drewtempelmeyer/palenight.vim.git
git clone https://tpope.io/vim/surround.git
cd ~/.vim/bundle/YouCompleteMe
./install --go-completer --ts-completer

echo "[+] Installing ZSH"
cd ~
sudo apt-get install -y zsh
git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"
# removing to have a clean first install
# echo 'source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"' >> .zshrc

# launch ufw
ufw enable

# dowload cryptostorm files
mkdir -p ~/Documents/vpn/
cd ~/Documents/vpn/
wget https://cryptostorm.is/configs/rsa/configs.zip
unzip configs.zip

echo "[+] Installing Ruby"
if [ -d ~/.rbenv ]; then
   echo "[*] You have Ruby with rbenv!"
else
   echo "[!] Installing Ruby..."
   rvm implode && sudo rm -rf ~/.rvm
   rm -rf ~/.rbenv/
   sudo apt install -y build-essential tklib zlib1g-dev libssl-dev libffi-dev libxml2 libxml2-dev libxslt1-dev libreadline-dev libssl1.0-dev
   sudo apt clean
   git clone https://github.com/rbenv/rbenv.git ~/.rbenv
   git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
   rbenv install 2.5.5
fi

echo "[*] Installing Go"
if [ -d ~/go ]; then
    echo "[*] You have Go!"
else
    echo "[!] Installing Go..."
    cd ~
    wget https://dl.google.com/go/go1.12.4.linux-amd64.tar.gzip
    tar -C /usr/local -xzf go1.12.4.linux-amd64.tar.gz
    echo "export PATH=$PATH:/usr/local/go/bin"  >> .zprofile
fi

# echo "[*] Installing NodeJS and NVM"
# # if [ -d ~/.nvm ]; then
    # echo "[*] You have NVM"
# else
    # echo "[!] Installing NVM..."
    # # TODO: keep track of the NVM version
    # curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
# fi

echo "[*] Install Python3"
if hash python3 2>/dev/null; then
    echo "[!] Installing python3"
    sudo apt install python3
else
    echo "[*] You have python3!"
fi

echo "[*] Installing Heroku"
sudo apt install snapd
sudo snap install --classic heroku

echo "[*] Installing visual candy"
sudo apt install -y papirus-icon-theme fonts-firacode
cd
bash -c  "$(wget -qO- https://git.io/vQgMr)"


echo "[+] Done!"
