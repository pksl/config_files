set nocompatible              " be iMproved, required
filetype off                  " required
filetype plugin indent on
:let mapleader=","

execute pathogen#infect()
set termguicolors
set guifont=Fira\ Code\ Retina\ 11
call pathogen#helptags()
" theme
syntax on
let ayucolor="mirage"

set laststatus=2
set backspace=indent,eol,start
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set autoindent
set textwidth=80
set autoread
set hlsearch
set incsearch
set nu

if v:version > 703 || v:version == 703 && has("patch541")
  set formatoptions+=j " Delete comment character when joining commented lines
endif



"set foldmethod=indent
>>>>>>> refs/remotes/origin/master
set hlsearch
set completefunc=syntaxcomplete#Complete
if has("multi_byte")
  set encoding=utf-8
  setglobal fileencoding=utf-8
else
  echoerr "Sorry, this version of (g)vim was not compiled with +multi_byte"
endif
let g:lightline = {
      \ 'colorscheme':'palenight',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'gitbranch#name'
      \ },
      \ }
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
inoremap <C-p> CtrlP<CR>
nnoremap <C-p> CtrlP<CR>
<<<<<<< HEAD
nnoremap <C-b> :NERDTreeToggle<CR>
inoremap <C-b> :NERDTreeToggle<CR>
let NERDTreeQuitOnOpen = 1
inoremap jj <ESC>:w<CR>
map <F7> gg=G<C-o><C-o>
map <F4> :noh<CR>
=======
nnoremap <C-b> :Vex 20<CR>
inoremap <C-b> :Vex 20<CR>
inoremap jj <ESC>:w<CR>
map <F7> gg=G<C-o><C-o>

>>>>>>> refs/remotes/origin/master
let g:user_emmet_mode='a'
let g:user_emmet_leader_key='<Tab>'
let g:user_emmet_settings= {
      \ 'javascript.jsx' : {
    \ 'extends': 'jsx',
     \ },
\}
nnoremap <C-c> :Goyo 120<CR>
inoremap <C-c> :Goyo 120<CR>
nmap <silent> <C-n> <Plug>(ale_previous_wrap)
nmap <silent> <C-m> <Plug>(ale_next_wrap)
map <F4> :nohl<CR>




" Set this variable to 1 to fix files when you save them.
let g:ale_fixers = {
      \   '*': ['remove_trailing_lines', 'trim_whitespace'],
      \   'css': ['prettier', 'stylelint'],
      \   'javascript': ['eslint', 'prettier'],
      \   'JSON': ['fixjson', 'jsonlint', 'prettier'],
      \   'SASS': ['sass-lint', 'stylelint'],
      \   'SCSS': ['prettier', 'sass-lint'],
      \   'YAML': ['prettier', 'yamlling'],
      \   'ruby': ['rubocop', 'rufo'],
      \   'python': ['autopep8', 'isort', 'yapf'],
      \   'HTML': ['HTMLHint', 'proselint'],
      \   'erb': ['erb'],
      \   'go': ['gofmt']
      \}
let g:ale_fix_on_save = 1

" Go specific helpers
let g:go_highlight_structs = 1
let g:go_highlight_methods = 1
let g:go_highlight_functions = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_function_parameters = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_types = 1
let g:go_hightlight_fields = 1
let g:go_highlight_generate_tags = 1
let g:go_highlight_variable_declarations = 1
let g:go_highlight_variable_assignments = 1

let g:go_fmt_command = "goimports"

<<<<<<< HEAD
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Use compact syntax for prettified multi-line comments
=======
" nerdCommenter
let g:NERDSpaceDelims = 1
>>>>>>> refs/remotes/origin/master
let g:NERDCompactSexyComs = 1
